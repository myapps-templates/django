FROM python:3
ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code

COPY . /code/
RUN pip3 install -r requirements.txt

ENV PORT 8000
EXPOSE $PORT
CMD [ "python3", "manage.py", "runserver", "8000" ]